#
# global vars
#
export PROJECT_ROOT=$(pwd)
export DIRENV_PATH="$PROJECT_ROOT/.direnv"
export TOOLS_PATH="$DIRENV_PATH/bin"

#
# helper
#
export IS_MAINTAINER="$(test -e $PROJECT_ROOT/.vault_pass && echo yes)"

#
# required
#
if test -z "$IS_MAINTAINER"; then
    echo -e "\e[33m💡 Create '.vault_pass' in project root '$PROJECT_ROOT' to activate Testing Farm maintainer access."
fi

#
# setup
#
if [ ! -e "$DIRENV_PATH" ]; then
  source setup/environment.sh
else
  echo -e "\e[32m💡 Use 'make clean; direnv allow' to reinstall\e[0m"
fi

#
# tools
#
PATH_add "$TOOLS_PATH"
PATH_add "$(poetry run bash -c 'echo $VIRTUAL_ENV')/bin"

#
# Configuration below is only for maintainers
#
test -z "$IS_MAINTAINER" && return

#
# export AWS credentials
#
export AWS_ACCESS_KEY_ID="$(ansible-vault view --vault-password-file .vault_pass secrets/credentials.yaml | yq -r .credentials.aws.fedora.access_key)"
export AWS_SECRET_ACCESS_KEY="$(ansible-vault view --vault-password-file .vault_pass secrets/credentials.yaml | yq -r .credentials.aws.fedora.secret_key)"
export AWS_DEFAULT_REGION="us-east-1"

#
# kubectl
#
export KUBECONFIG="$DIRENV_PATH/.kube/config"
export KREW_ROOT="$DIRENV_PATH/.krew/"
PATH_add "$DIRENV_PATH/.krew/bin"

#
# terraform
#
export TF_TOKEN_app_terraform_io="$(ansible-vault view --vault-password-file .vault_pass secrets/credentials.yaml | yq -r .credentials.terraform.cloud.testing_farm_bot.token)"
export TF_VAR_ansible_vault_password_file="$PROJECT_ROOT/.vault_pass"
export TF_VAR_ansible_vault_credentials="credentials.yaml"
export TF_VAR_ansible_vault_secrets_root="$PROJECT_ROOT/secrets"
export TF_VAR_gitlab_testing_farm_bot="$(ansible-vault view --vault-password-file .vault_pass secrets/credentials.yaml | yq -r .credentials.gitlab.testing_farm_bot_terragrunt.token)"

#
# Testing Farm tokens
#
export TESTING_FARM_API_TOKEN_PUBLIC="$(ansible-vault view --vault-password-file .vault_pass secrets/credentials.yaml | yq -r .credentials.testing_farm.ranch.public.bot_api_key)"
# NOTE: this is not used currently, but we plan to use it later
export TESTING_FARM_API_TOKEN_REDHAT="$(ansible-vault view --vault-password-file .vault_pass secrets/credentials.yaml | yq -r .credentials.testing_farm.ranch.redhat.bot_api_key)"

#
# gitlab-ci-linter
#
export GITLAB_PRIVATE_TOKEN="$(ansible-vault view --vault-password-file .vault_pass secrets/credentials.yaml | yq -r .credentials.gitlab.testing_farm_bot.token)"
